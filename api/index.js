let express = require('express');
let bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/', function(req, res) {
  res.json({jsonrpc: '2.0', result: '0.0.1', id: req.body.id});
});

app.post('/api/auth', function(req, res) {
  const streamKey = req.body.name;
  return res.status(200).json({verified: true});
});

app.post('/api/done', function(req, res) {
  const streamKey = req.body.name;
  return res.json({done: true});
});

app.post('/users', function(req, res) {
  const username = req.body.username;
  const verified = req.body.verified;
});

app.listen(8080, function() {
  console.log('Running')
});
